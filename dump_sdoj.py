# Copyright (c) 2020 Enrico Pozzobon <enrico@epozzobon.it>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from io import FileIO
from msilib.schema import File
import struct
import sys
import json
import os
import tempfile
import hashlib
import glob
import ctypes
from typing import Any, Optional, Dict


if getattr(sys, 'frozen', False):
    application_path = sys._MEIPASS
else:
    application_path = os.path.dirname(os.path.abspath(__file__))
dll_path = os.path.join(application_path, "bin/cv1k-compression.dll")
cv1k_compression_dll = ctypes.WinDLL(dll_path)
cv1k_extract_proto = ctypes.WINFUNCTYPE(
    ctypes.c_long,
    ctypes.c_void_p,
    ctypes.c_void_p,
    ctypes.POINTER(ctypes.c_ulong))
cv1k_deflate_proto = ctypes.WINFUNCTYPE(
    ctypes.c_long,
    ctypes.c_void_p,
    ctypes.c_void_p,
    ctypes.c_ulong)
cv1k_extract_params = (1, "p1", 0), (1, "p2", 0), (1, "p3",0)
cv1k_deflate_params = (1, "p1", 0), (1, "p2", 0), (1, "p3",0)

cv1k_extract = cv1k_extract_proto(("cv1k_extract", cv1k_compression_dll), cv1k_extract_params)
cv1k_deflate = cv1k_deflate_proto(("cv1k_deflate", cv1k_compression_dll), cv1k_deflate_params)



def decompress(buffer):
    uncompressed_size, bytes_left, data_bytes_off = struct.unpack(">III", buffer[:12])
    c_extracted = (ctypes.c_ubyte * uncompressed_size)()
    c_extracted_size = (ctypes.c_ulong * 1)()
    c_extracted_size[0] = len(buffer)
    cv1k_extract(c_extracted, buffer, c_extracted_size)
    c_extracted = bytes(c_extracted)
    return c_extracted


def compress(buffer):
    c_deflated = (ctypes.c_ubyte * (30 + len(buffer) // 8 * 9))()
    c_extracted_size = len(buffer)
    deflated_size = cv1k_deflate(c_deflated, buffer, len(buffer))
    c_deflated = bytes(c_deflated)
    return c_deflated[:deflated_size]


def htons(x):
    return struct.unpack("<H", struct.pack(">H", x))[0]


def parse_toc(f: FileIO, print=print) -> Optional[Dict[str, Any]]:
    file_hdr = f.read(0x20)
    file_hdr = struct.unpack(">IIIIIIII", file_hdr)
    ( field_0, field_4, data_offset, files_count,
        field_10, field_14, field_18, field_1c
    ) = file_hdr
    if field_0 != 3221815575:
        return None

    out_table = {
        'source': f.name,
        'header': file_hdr,
        'toc': []
    }

    for idx in range(files_count):
        res_hdr = f.read(0x114)
        res_hdr = struct.unpack(">IHHIII256s", res_hdr)
        res_hdr = list(res_hdr)
        assert res_hdr.pop(0) == 0
        res_hdr[0] = htons(res_hdr[0])
        assert idx == res_hdr[0]
        res_hdr[-1] = res_hdr[-1].decode('ascii').replace("\0", "")
        print([hex(d) for d in res_hdr[:-1]])
        (idx, flags, res_size_u, res_size, res_offset, path) = res_hdr
        out_table['toc'].append({
            'index': idx,
            'flags': flags,
            'size': res_size_u,
            'compressed': res_size,
            'offset': res_offset,
            'path': path
        })

    return out_table


def dump(f, root_path, print=print):
    out_table = parse_toc(f, print)
    if out_table is None:
        return None

    for element in out_table['toc']:
        res_size_u = element['size']
        res_size = element['compressed']
        res_offset = element['offset']
        path = element['path']

        print("0x%x : 0x%x, 0x%x : %s" % (res_offset, res_size, res_size_u, path))
        if res_size == 0:
            read_size = res_size_u
            compressed = False
        else:
            read_size = res_size
            compressed = True
        f.seek(res_offset, 0)
        dst_path = os.path.join(root_path, path)
        directory = os.path.dirname(dst_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        res_bytes = f.read(read_size)

        if compressed:
            res_bytes = decompress(res_bytes)

        md5hash = hashlib.md5(res_bytes).hexdigest()
        element['md5'] = md5hash

        with open(dst_path, 'wb') as out:
            out.write(res_bytes)

        element['destination'] = dst_path

    return out_table


def pack(fp, info, root_path) -> None:
    toc = info['toc']
    hdr = info['header']
    source = info['source']
    files_count = hdr[3]
    data_offset = hdr[2]
    current_offset = data_offset

    fp.write(b"\0" * data_offset)
    fp.seek(0)
    fp.write(struct.pack(">IIIIIIII", *hdr))

    for res_hdr in toc:
        idx = res_hdr['index']
        flags = res_hdr['flags']
        res_size_u = res_hdr['size']
        res_size = res_hdr['compressed']
        res_offset = res_hdr['offset']
        path = res_hdr['path']
        old_md5hash = res_hdr['md5']

        with open(os.path.join(root_path, path), "rb") as ffp:
            data = ffp.read()

        new_size = len(data)
        if res_size != 0:
            # Compression is used, but was the file modified?
            modified = True
            if res_size_u == new_size:
                new_md5hash = hashlib.md5(data).hexdigest()
                if new_md5hash == old_md5hash:
                    modified = False

            if modified:
                data = compress(data)
                res_size = len(data)
            else:
                with open(source, 'rb') as fsrc:
                    fsrc.seek(res_offset, 0)
                    data = fsrc.read(res_size)

        orig = fp.tell()
        fp.seek(current_offset)
        new_offset = current_offset
        fp.write(data)
        current_offset = fp.tell()
        fp.seek(orig)

        path = (path + "\0" * 256)[:256].encode('ascii')
        res_hdr = 0, htons(idx), flags, new_size, res_size, new_offset, path
        fp.write(struct.pack(">IHHIII256s", *res_hdr))


def dump_game(source_dir, output_dir, print=print):
    paths = glob.glob(os.path.join(source_dir, "*.bin"))
    output_root = os.path.join(output_dir, 'game')

    table = {}
    for path in paths:
        print(path)
        with open(path, 'rb') as f:
            toc = dump(f, output_root, print=print)
            if toc is not None:
                table[os.path.basename(path)] = toc

    with open(os.path.join(output_dir, "info.json"), "w") as fp:
        json.dump(table, fp, indent=2)


def pack_game(source_dir, output_dir, print=print):
    source_root = os.path.join(source_dir, 'game')

    with open(os.path.join(source_dir, "info.json"), "r") as fp:
        table = json.load(fp)
    
    for fname in table:
        toc = table[fname]
        path = os.path.join(output_dir, fname)
        if os.path.exists(path):
            os.unlink(path)
        print("Packing %s... " % path)
        with open(path, "w+b") as fp:
            pack(fp, toc, source_root)
        print("Done!")
