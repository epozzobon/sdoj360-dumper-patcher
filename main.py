import os
import os.path
import subprocess
import tkinter
from tkinter import filedialog, Tk, ttk, scrolledtext
import threading
import glob
import sys
import tempfile
import json
import queue
from typing import Optional
import dump_sdoj
import struct


SAIDAIOUJOU_PATH = r"D:\games\xenia\Dodonpachi SaiDaiOuJou"
SUSDIEOUJOU_PATH = r"D:\games\xenia\Tatsujin Doh SusDieOuJou"


if getattr(sys, 'frozen', False):
    application_path = sys._MEIPASS
else:
    application_path = os.path.dirname(os.path.abspath(__file__))
xma2encode_path = os.path.join(application_path, "bin/xma2encode.exe")


class BackgroundThread(threading.Thread):
    def __init__(self, my_queue: queue.Queue):
        threading.Thread.__init__(self)
        self.cancellation_requested = False
        self.exception = None
        self.queue = my_queue

    def cancel(self):
        self.cancellation_requested = True

    def print(self, str):
        if self.cancellation_requested:
            raise Exception("Thread was cancelled")
        self.queue.put(("print", str))

    def run(self):
        try:
            self._run()
        except Exception as ex:
            self.exception = ex
            raise ex
        finally:
            self.queue.put(("end", self.exception))


class ExtractorThread(BackgroundThread):
    def __init__(self, my_queue: queue.Queue) -> None:
        BackgroundThread.__init__(self, my_queue)
        self.input_path: Optional[str] = None
        self.output_path: Optional[str] = None

    def _run(self):
        paths = glob.glob(os.path.join(self.input_path, "*.bin"))
        output_root = os.path.join(self.output_path, 'game')

        table = {}
        for i, path in enumerate(paths):
            if self.cancellation_requested:
                return
            self.queue.put(("progress", i/len(paths)))
            with open(path, 'rb') as f:
                toc = dump_sdoj.dump(f, output_root, print=self.print)
                if toc is not None:
                    table[os.path.basename(path)] = toc
        self.queue.put(("progress", 1))

        with open(os.path.join(self.output_path, "info.json"), "w") as fp:
            json.dump(table, fp, indent=2)
        self.print("Done!")


class ConverterThread(BackgroundThread):
    def __init__(self, my_queue):
        BackgroundThread.__init__(self, my_queue)
        self.path = None
        self.conversion = None

    def _run(self):
        paths = []
        for dirpath, _, filenames in os.walk(self.path):
            for name in filenames:
                path = os.path.join(dirpath, name)
                if self.cancellation_requested:
                    return
                if self.conversion == 'xma2wav':
                    if name.endswith(".xma"):
                        paths.append(path)
                if self.conversion == 'wav2xma':
                    if name.endswith(".wav"):
                        paths.append(path)

        for i, path in enumerate(paths):
            self.queue.put(("progress", i/len(paths)))
            self.print(path)
            if self.conversion == 'xma2wav':
                p = subprocess.Popen([
                    xma2encode_path, path,
                    '/DecodeToPCM', f'{path[:-4]}.wav'], shell=True)
                assert 0 == p.wait()
                with open(f'{path[:-4]}.wav', 'rb') as w:
                    chunkID, chunkSize, riffType = struct.unpack("<III", w.read(12))
                    fmtTag, fmtLength = struct.unpack("<II", w.read(8))
                    fmt = w.read(fmtLength)
                    data = w.read()
                with open(f'{path[:-4]}.wav', 'wb') as w:
                    w.write(struct.pack("<III", chunkID, 28 + len(data), riffType))
                    w.write(struct.pack("<II", fmtTag, 16))
                    w.write(fmt[:16])
                    w.write(data)

            if self.conversion == 'wav2xma':
                p = subprocess.Popen([
                    xma2encode_path, path,
                    '/TargetFile', f'{path[:-4]}.xma'], shell=True)
                assert 0 == p.wait()

        self.queue.put(("progress", 1))
        self.print("Done!")


class PatcherThread(BackgroundThread):
    def __init__(self, my_queue):
        BackgroundThread.__init__(self, my_queue)
        self.input_path = None
        self.output_path = None

    def _run(self):
        os.makedirs(self.output_path, exist_ok=True)
        source_root = os.path.join(self.input_path, 'game')

        with open(os.path.join(self.input_path, "info.json"), "r") as fp:
            table = json.load(fp)
        
        for i, fname in enumerate(table):
            if self.cancellation_requested:
                return
            self.queue.put(("progress", i/len(table)))
            toc = table[fname]
            path = os.path.join(self.output_path, fname)
            if os.path.exists(path):
                os.unlink(path)
            self.print("Packing %s... " % path)
            with open(path, "w+b") as fp:
                dump_sdoj.pack(fp, toc, source_root)
            self.print("Done!")
        self.queue.put(("progress", 1))


class PatchApp:
    STATE_IDLE = object()
    STATE_DUMPING = object()
    STATE_PACKING = object()

    def __init__(self):
        self.global_queue = queue.Queue()

        self.state = PatchApp.STATE_IDLE

        self.worker_thread = None
        self.root = Tk()
        self.root.title("Tatsujin D'oh Sus Die Ou Jou - Patcher")
        self.input_path_var = tkinter.StringVar()
        self.input_path_var.set(SAIDAIOUJOU_PATH)
        self.extracted_path_var = tkinter.StringVar()
        self.extracted_path_var.set(os.path.join(tempfile.gettempdir(), "sdoj"))
        self.output_path_var = tkinter.StringVar()
        self.output_path_var.set(SUSDIEOUJOU_PATH)

        self.frm = ttk.Frame(self.root, padding=10, width=800, height=560)
        self.frm.grid()
        self.frm.grid_columnconfigure(0, weight=1)

        self.lb1 = ttk.Label(self.frm, text="Path to DoDonPachi SaiDaiOuJou (RGH):", width=50)
        self.lb1.grid(column=0, row=0, columnspan=6, sticky='EW')
        
        self.tx1 = ttk.Entry(self.frm, textvariable=self.input_path_var)
        self.tx1.grid(column=0, row=1, columnspan=5, sticky='EW')

        self.bt1 = ttk.Button(self.frm, text="Browse...", command=self.browse_input_dir)
        self.bt1.grid(column=5, row=1)

        self.lb3 = ttk.Label(self.frm, text="Directory for extracted files:", width=50)
        self.lb3.grid(column=0, row=2, columnspan=6, sticky='EW')

        self.tx3 = ttk.Entry(self.frm, textvariable=self.extracted_path_var)
        self.tx3.grid(column=0, row=3, columnspan=5, sticky='EW')

        self.bt4 = ttk.Button(self.frm, text="Browse...", command=self.browse_extracted_dir)
        self.bt4.grid(column=5, row=3)

        self.lb5 = ttk.Label(self.frm, text="Directory for patched game:", width=50)
        self.lb5.grid(column=0, row=4, columnspan=6, sticky='EW')

        self.tx4 = ttk.Entry(self.frm, textvariable=self.output_path_var)
        self.tx4.grid(column=0, row=5, columnspan=5, sticky='EW')

        self.bt6 = ttk.Button(self.frm, text="Browse...", command=self.browse_output_dir)
        self.bt6.grid(column=5, row=5)

        self.lb2 = ttk.Label(self.frm, text="0%")
        self.lb2.grid(column=0, row=6, columnspan=6)

        self.pb1 = ttk.Progressbar(self.frm, orient=tkinter.HORIZONTAL, length=200)
        self.pb1.grid(column=0, row=7, columnspan=6, sticky='EW')

        self.tx2 = scrolledtext.ScrolledText(self.frm)
        self.tx2.grid(column=0, row=8, columnspan=6, sticky='NSEW')
        self.frm.grid_rowconfigure(8, weight=1)

        self.bt5 = ttk.Button(self.frm, text="Extract", command=self.do_extract)
        self.bt5.grid(column=1, row=9)
        
        self.bt7 = ttk.Button(self.frm, text="XMA -> WAV", command=self.xma2wav)
        self.bt7.grid(column=2, row=9)
        
        self.bt8 = ttk.Button(self.frm, text="WAV -> XMA", command=self.wav2xma)
        self.bt8.grid(column=3, row=9)

        self.bt2 = ttk.Button(self.frm, text="Patch", command=self.do_patch)
        self.bt2.grid(column=4, row=9)

        self.bt3 = ttk.Button(self.frm, text="Quit", command=self.root.destroy)
        self.bt3.grid(column=5, row=9)
        self.frm.pack(expand=True)

        self.lb4 = ttk.Label(self.frm, text="Ready")
        self.lb4.grid(column=0, row=10, columnspan=6, sticky='EW')

        self.bt2['state'] = "normal" if os.path.exists(self.input_path_var.get()) else "disabled"
        self.frm.grid_propagate(0)

    def set_state(self, state):
        if state == PatchApp.STATE_IDLE:
            self.bt1['state'] = "enabled"
            self.bt2['state'] = "enabled"
            self.bt3['state'] = "enabled"
            self.bt4['state'] = "enabled"
            self.bt5['state'] = "enabled"
            self.bt6['state'] = "enabled"
            self.bt7['state'] = "enabled"
            self.bt8['state'] = "enabled"
        elif state == PatchApp.STATE_DUMPING or state == PatchApp.STATE_PACKING:
            if self.state != PatchApp.STATE_IDLE:
                raise Exception("Bad state transition")
            self.bt1['state'] = "disabled"
            self.bt2['state'] = "disabled"
            self.bt3['state'] = "disabled"
            self.bt4['state'] = "disabled"
            self.bt5['state'] = "disabled"
            self.bt6['state'] = "disabled"
            self.bt7['state'] = "disabled"
            self.bt8['state'] = "disabled"
            self.tx2.delete('1.0', tkinter.END)
        else:
            raise Exception("Bad state transition")
        self.state = state

    def check_queue(self):
        try:
            while 1:
                evt, value = self.global_queue.get(False)
                if evt == "print":
                    self.lb4['text'] = value
                    self.tx2.insert(tkinter.END, value)
                    self.tx2.insert(tkinter.END, "\r\n")
                    self.tx2.see("end")
                elif evt == "progress":
                    self.pb1['value'] = value * self.pb1['max']
                    self.lb2['text'] = "%.1f%%" % (value * 100)
                elif evt == "error":
                    self.lb2['text'] = value
                    self.tx2.insert(tkinter.END, value)
                    self.tx2.insert(tkinter.END, "\r\n")
                    self.tx2.see("end")
                elif evt == "end":
                    self.lb2['text'] = str(value)
                    self.worker_thread.join()
                    self.worker_thread = None
                    self.set_state(PatchApp.STATE_IDLE)
        except queue.Empty:
            self.root.after(10, self.check_queue)

    def browse_input_dir(self):
        current = self.input_path_var.get()
        f = filedialog.askdirectory(initialdir=current)
        if f is not None and os.path.isdir(f):
            self.input_path_var.set(f)

    def browse_extracted_dir(self):
        current = self.extracted_path_var.get()
        f = filedialog.askdirectory(initialdir=current)
        if f is not None and os.path.isdir(f):
            self.extracted_path_var.set(f)

    def browse_output_dir(self):
        current = self.output_path_var.get()
        f = filedialog.askdirectory(initialdir=current)
        if f is not None and os.path.isdir(f):
            self.output_path_var.set(f)

    def do_patch(self):
        self.set_state(PatchApp.STATE_PACKING)
        self.worker_thread = PatcherThread(self.global_queue)
        self.worker_thread.input_path = self.extracted_path_var.get()
        self.worker_thread.output_path = self.output_path_var.get()
        self.worker_thread.start()

    def do_extract(self):
        self.set_state(PatchApp.STATE_DUMPING)
        self.worker_thread = ExtractorThread(self.global_queue)
        self.worker_thread.input_path = self.input_path_var.get()
        self.worker_thread.output_path = self.extracted_path_var.get()
        self.worker_thread.start()

    def wav2xma(self):
        self.set_state(PatchApp.STATE_DUMPING)
        self.worker_thread = ConverterThread(self.global_queue)
        self.worker_thread.path = self.extracted_path_var.get()
        self.worker_thread.conversion = "wav2xma"
        self.worker_thread.start()

    def xma2wav(self):
        self.set_state(PatchApp.STATE_DUMPING)
        self.worker_thread = ConverterThread(self.global_queue)
        self.worker_thread.path = self.extracted_path_var.get()
        self.worker_thread.conversion = "xma2wav"
        self.worker_thread.start()

    def run(self):
        self.root.after(10, self.check_queue)
        self.root.mainloop()
        if self.worker_thread is not None and self.worker_thread.is_alive():
            self.worker_thread.cancel()
            print("Patch cancelled")
            self.worker_thread.join()
        

if __name__ == '__main__':
    app = PatchApp()
    app.run()